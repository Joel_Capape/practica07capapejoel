package ficherosSecuencial;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class FicherosSecuencial implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String MENSAJE = "Introduce el nombre del fichero";
	String archivoNombre;
	ArrayList<Jugadores> listaJugadores;
	public static Scanner input = new Scanner(System.in);

	public FicherosSecuencial() {
		this.listaJugadores = new ArrayList<Jugadores>();
	}

//public crear fichero, control de excepciones checked IOEEXCEPTION
	public void crearFichero() {
		File archivo = new File(archivoNombre = input.nextLine());
		if (archivo.exists()) {
			System.out.println("El fichero ya existe");
		} else {
			String fin;
			do {
				darAltaJugador();
				System.out.println("¿Quierés introducir otro jugador?Si/No");
				fin = input.nextLine();
			} while (!fin.equalsIgnoreCase("no"));
		}

	}

//public crear lista de jugadores

	// FUNCIONALIDAD EXTRA CONTROL DE EXCEPCIONES UNCHECKED
	public double rellenarSueldo() {
		boolean controlador = false;
		double precio = 0;
		do {
			try {
				System.out.println("Introduce el sueldo del jugador");
				precio = input.nextDouble();
				controlador = false;
			} catch (InputMismatchException a) {
				System.out.println("El sueldo tiene un formato no válido");
				controlador = true;
				input.nextLine();
			}
		} while (controlador);
		return precio;
	}

	public void darAltaJugador() {
		String fecha = LocalDate.now().toString();
		System.out.println("Introduce el nombre del jugador");
		String nombre = input.nextLine();
		System.out.println("Introduce el apellido del jugador");
		String apellido = input.nextLine();
		System.out.println("Introduce la edad del jugador");
		int edad = input.nextInt();
		double sueldo = rellenarSueldo();
		input.nextLine();
		System.out.println("La posición en la que puede jugar son: Portero, Defensa, Mediocentro o Delantero");
		String posicion = input.nextLine();
		System.out.println("Introduce el número de goles del jugador");
		int nGoles = input.nextInt();
		input.nextLine();
		listaJugadores.add(new Jugadores(fecha, nombre, apellido, edad, sueldo, posicion, nGoles));
	}

//guardar en fichero, control de excepciones checked IOEEXCEPTION
	public void guardarFichero() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File(archivoNombre)));
			escritor.writeObject(listaJugadores);
			escritor.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("No se ha encontrado el fichero");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida ");
		}
	}

//visualizar en fichero
	@SuppressWarnings("unchecked")
	public void visualizarFichero() {
		ArrayList<Jugadores> jugador;
		try {
			System.out.println("Visualizamos los datos del fichero " + archivoNombre);
			ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			System.out.println(jugador);
			entrada.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("No se encuentra el archivo");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}
	}

	// metodo buscar en fichero por nombre al jugador
	@SuppressWarnings("unchecked")
	public Jugadores buscarPalabra() {

		System.out.println("Introduce el nombre del jugador que quieres buscar");
		String nombreJugador = input.nextLine();
		ArrayList<Jugadores> jugador;
		try {
			@SuppressWarnings("resource")
			ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			for (int i = 0; i < jugador.size(); i++) {
				Jugadores jugadores = jugador.get(i);
				if (jugador != null && jugadores.getNombre().equals(nombreJugador)) {
					return jugadores;
				}
			}
			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}

		return null;

	}

//METODOS EXTRAS
//fecha numero en mes y cambiarlo a letras
	@SuppressWarnings("unchecked")
	public void cambiarMesAletras() {

		try {
			ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			ArrayList<Jugadores> jugador;
			String fecha = "";
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			for (int i = 0; i < jugador.size(); i++) {
				fecha = jugador.get(i).getFechaDeAlta();
			}
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy");
			LocalDate date = LocalDate.parse(fecha);
			String fechaCambiada = date.format(formatter);
			for (int i = 0; i < jugador.size(); i++) {
				Jugadores jugadores = jugador.get(i);
				if (jugador != null && jugadores.getFechaDeAlta().equals(fecha)) {
					listaJugadores = jugador;
					listaJugadores.get(i).setFechaDeAlta(fechaCambiada);

				}
			}
			System.out.println("Se ha actualizado la fecha");
			guardarFichero();
			System.out.println("La informacion se ha guardado");
			visualizarFichero();

			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}

	}

//mostrar el jugador con más goles
	@SuppressWarnings("unchecked")
	public void maximoGoleador() {

		try {
			ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			ArrayList<Jugadores> jugador;
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			int[] arrayGoles = new int[jugador.size()];
			String[] nombres = new String[jugador.size()];
			int mayor = 0;
			int posicion = 0;
			String nombre = "";
			for (int i = 0; i < arrayGoles.length; i++) {
				arrayGoles[i] = jugador.get(i).getnGoles();
			}
			for (int i = 0; i < nombres.length; i++) {
				nombres[i] = jugador.get(i).getNombre();
			}
			for (int i = 0; i < arrayGoles.length; i++) {
				if (mayor < arrayGoles[i]) {
					mayor = arrayGoles[i];
					posicion = i;
				}
			}
			for (int i = 0; i < nombres.length; i++) {
				if (posicion == i) {
					nombre = nombres[i];
				}
			}
			System.out.println("El máximo goleador del fichero " + archivoNombre);
			System.out.println("El jugador " + nombre + " ha marcado " + mayor + " goles");
			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}

	}

//mostrar el nombre de los jugadores que estan entrenando
	@SuppressWarnings("unchecked")
	public void entrenamiento() {

		try {
			ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			ArrayList<Jugadores> jugador;
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			Jugadores[] vectorJugadores = new Jugadores[jugador.size()];
			int numeroAleatorio = (int) (Math.random() * 5);
			boolean estaEntrenando = false;
			jugador.toArray(vectorJugadores);
			for (int i = 0; i < jugador.size(); i++) {
				vectorJugadores[i] = jugador.get(i);
			}
			for (int i = 0; i < vectorJugadores.length; i++) {
				if (i == numeroAleatorio) {
					estaEntrenando = true;
				} else {
					estaEntrenando = false;
				}
			}
			if (estaEntrenando) {
				System.out.println("Los jugadores están entrenando");
			} else {
				System.out.println("Los jugadores tienen descanso");
			}
			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}

	}

//mostrar la media total de goles de todos los jugadores
	@SuppressWarnings("unchecked")
	public void mediaGoles() {

		try {
			ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			ArrayList<Jugadores> jugador;
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			int suma = 0;
			double media = 0;
			for (int i = 0; i < jugador.size(); i++) {
				suma = suma + jugador.get(i).getnGoles();
			}
			media = (double) suma / (double) jugador.size();
			System.out.println("La media de goles entre todos los jugadores es " + media);
			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}
	}

//mostrar el nombre de aquellos jugadores que superan los 18 años
	@SuppressWarnings("unchecked")
	public void mostrarMayoresDeEdad() {
		ObjectInputStream entrada;

		try {
			entrada = new ObjectInputStream(new FileInputStream(archivoNombre));
			System.out.println("Los jugadores mayores de edad son: ");
			ArrayList<Jugadores> jugador;
			jugador = (ArrayList<Jugadores>) entrada.readObject();
			for (int i = 0; i < jugador.size(); i++) {
				if (jugador != null && jugador.get(i).getEdad() > 18) {
					System.out.print("Nombre: " + jugador.get(i).getNombre() + "\n");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error de entrada salida");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error en tiempo de ejecución");
		}

	}

}
