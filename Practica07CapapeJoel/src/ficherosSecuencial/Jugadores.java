package ficherosSecuencial;

import java.io.Serializable;

public class Jugadores implements Serializable {
	private static final long serialVersionUID = 1L;
	String fechaDeAlta;
	String nombre;
	String apellidos;
	int edad;
	double sueldo;
	String posicion;
	int nGoles;

	public Jugadores() {
		this.nombre = "";
		this.apellidos = "";
		this.edad = 0;
		this.sueldo = 0.0;
		this.posicion = "";
		this.nGoles = 0;

	}

	public Jugadores(String fechaDeAlta, String nombre, String apellidos, int edad, double sueldo, String posicion,
			int nGoles) {
		this.fechaDeAlta = fechaDeAlta;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.sueldo = sueldo;
		this.posicion = posicion;
		this.nGoles = nGoles;
	}

	public String getFechaDeAlta() {
		return fechaDeAlta;
	}

	public void setFechaDeAlta(String fechaDeAlta) {
		this.fechaDeAlta = fechaDeAlta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public int getnGoles() {
		return nGoles;
	}

	public void setnGoles(int nGoles) {
		this.nGoles = nGoles;
	}

	@Override
	public String toString() {
		return "Datos Jugador " + " = " + "fechaDeAlta=" + fechaDeAlta + ", nombre=" + nombre + ", apellidos="
				+ apellidos + ", edad=" + edad + ", sueldo=" + sueldo + ", posicion=" + posicion + ", nGoles=" + nGoles
				+ "\n";
	}

}
