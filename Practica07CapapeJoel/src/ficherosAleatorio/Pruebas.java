package ficherosAleatorio;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class Pruebas {
	String archivo;

	public Pruebas(String nombre) {
		this.archivo = nombre;
	}

	// int 4 bytes
	// double 8 bytes
	// cadenas longitud+ 2 bytes

	public Pruebas() {
		// TODO Auto-generated constructor stub
	}

	public void rellenarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Introduce el nombre del fichero");
			RandomAccessFile f = new RandomAccessFile(archivo = in.readLine(), "rw");
			f.seek(f.length());
			String fin = "";
			do {
				System.out.println("Introduce el nombre del entrenador");
				String nombre = in.readLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("Introduce los apellidos del entrenador");
				String apellidos = in.readLine();
				apellidos = formatearNombre(apellidos, 20);
				f.writeUTF(apellidos);
				System.out.println(
						"El estilo de juego que puede jugar es: Ofensivo, UltraOfensivo, Defensivo, UltraDefensivo");
				String estiloDeJuego = in.readLine();
				estiloDeJuego = formatearNombre(estiloDeJuego, 20);
				f.writeUTF(estiloDeJuego);
				System.out.println("Introduce su equipo actual");
				String equipo = in.readLine();
				equipo = formatearNombre(equipo, 20);
				f.writeUTF(equipo);
				System.out.println("¿Quieres introducir otro entrenador?Si/No");
				fin = in.readLine();
			} while (!fin.equalsIgnoreCase("no"));
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	private String formatearNombre(String nombre, int lon) {
		// convertir la cadena al tama�o indicado
		if (nombre.length() > lon) {
			// recorta
			return nombre.substring(0, lon);
		} else {
			// completar con espacios en blanco
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
		}
		return nombre;
	}

	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			String apellidos = "";
			String estiloDeJuego = "";
			String equipo = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					apellidos = f.readUTF();
					estiloDeJuego = f.readUTF();
					equipo = f.readUTF();
					System.out.println("Nombre: " + nombre);
					System.out.println("Apellidos: " + apellidos);
					System.out.println("Estilo De Juego: " + estiloDeJuego);
					System.out.println("Equipo: " + equipo);
					System.out.println("\n");
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	public void modificarArchivo() {
		try {
			// pido el nombre del valor a buscar
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Nombre a modificar");
			String oldName = in.readLine();
			System.out.println("Dame el nuevo nombre");
			String newName = in.readLine();
			// abro el archivo
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			// busco en el archivo y cuando lo encuentre lo modifico
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(oldName)) {
						// tengo que modificarlo
						// 20+2 (cadena de 20 + 2 bytes) -> cadena + 2 bytes
						f.seek(f.getFilePointer() - 22);
						newName = formatearNombre(newName, 20);
						f.writeUTF(newName);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no está en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	// METODOS EXTRAS
	// Visualizar el numero de veces que aparece el estilo defensivo
	public void contadorDefensivo() {
		try {

			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String estiloDeJuego;
			String estilo = "Defensivo";
			int contador = 0;
			boolean finFichero = false;
			do {
				try {
					estiloDeJuego = f.readUTF();
					if (estiloDeJuego.trim().equalsIgnoreCase(estilo)) {
						contador++;
					}
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
			System.out.println("Hay " + contador + " con estilo Defensivo en el archivo");
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	// Copiar los entrenadores que su estilo de juego sea Ofensivo a un fichero que
	// se llamara EntrenadoresOfensivos
	public void copiarFichero() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			RandomAccessFile destino = new RandomAccessFile("EntrenadoresOfensivos", "rw");
			System.out.println("Introduce el estilo que quieres guardar en el fichero EntrenadoresOfensivos");
			String estilo = in.readLine();
			String nombre = "";
			String apellidos = "";
			String estiloDeJuego = "";
			String equipo = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					apellidos = f.readUTF();
					estiloDeJuego = f.readUTF();
					equipo = f.readUTF();
					if (estiloDeJuego.trim().equalsIgnoreCase(estilo)) {
						destino.writeUTF(nombre);
						destino.writeUTF(apellidos);
						destino.writeUTF(equipo);
					}
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
					destino.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	// Muestrame los entrenadores que tienen un estilo de juego X
	public void mostrarEstiloDeJuego() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			RandomAccessFile destino = new RandomAccessFile("EntrenadoresOfensivos", "rw");
			System.out.println("Introduce el estilo que quieres mostrar");
			String estilo = in.readLine();
			String nombre = "";
			String apellidos = "";
			String estiloDeJuego = "";
			String equipo = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					apellidos = f.readUTF();
					estiloDeJuego = f.readUTF();
					equipo = f.readUTF();
					if (estiloDeJuego.trim().equalsIgnoreCase(estilo)) {
						System.out.println(
								"Entrenadores con ese estilo de juego " + nombre + "" + apellidos + "" + equipo);
					}
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
					destino.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	// Si el apellido coincide por el que le pasamos por parametros remplazarlo por
	// *
	public void reemplazarPorAsterisco() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			System.out.println("Introduce el apellido del entrenador");
			String apellido = in.readLine();
			String apellidos = "";
			String cambio = "*************";
			boolean finFichero = false;
			System.out.println("Cambio el apellido del entrenador por *");
			do {
				try {
					apellidos = f.readUTF();
					if (apellidos.trim().equalsIgnoreCase(apellido)) {
						f.seek(f.getFilePointer() - 22);
						cambio = formatearNombre(cambio, 20);
						f.writeUTF(cambio);
					}
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}

	// codificar el nombre de un entrenador que le pasamos por parametro con
	// numerosAleatorios.
	public void codificarNombre() {
		
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			System.out.println("Introduce el nombre del Entrenador");
			String nombre = in.readLine();
			String nombreEntrenador = "";
			int n=0;
			String term="";
			boolean finFichero = false;
			System.out.println("C�digo Ascii de cada car�cter");
			for(int i=0;i<=nombre.length();i++) {
				double aleatorios=Math.random()*(20-5)+5;
				 n=(int)aleatorios;
				 term=term.concat(String.valueOf(n));
			}
			do {
				try {
					nombreEntrenador = f.readUTF();
					if (nombreEntrenador.trim().equalsIgnoreCase(nombre)) {
						f.seek(f.getFilePointer() - 22);
						term = formatearNombre(term, 20);
						f.writeUTF(term);
					}
				} catch (EOFException e) {
					System.out.println("\n");
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}
			

	// usar getBytes para conseguir el codigo ascii de cada caracter de un campo a
	// elegir
	public void codigoAscii() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			System.out.println("Introduce el nombre del equipo");
			String equipoB = in.readLine();
			String equipo = "";
			byte[] vectorBytes;
			boolean finFichero = false;
			System.out.println("C�digo Ascii de cada car�cter");
			do {
				try {
					equipo = f.readUTF();
					if (equipo.trim().equalsIgnoreCase(equipoB)) {
						vectorBytes = equipo.getBytes();
						for (int i = 0; i < vectorBytes.length; i++) {
							System.out.print(vectorBytes[i] + " ");
						}
					}
				} catch (EOFException e) {
					System.out.println("\n");
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}
}
