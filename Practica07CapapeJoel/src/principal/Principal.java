package principal;

import java.util.Scanner;

import ficherosAleatorio.FicherosAleatorios;
import ficherosSecuencial.FicherosSecuencial;

public class Principal {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion;
		FicherosSecuencial fichero = new FicherosSecuencial();
		FicherosAleatorios archivos = new FicherosAleatorios();
		do {
			System.out.println("*******************************************");
			System.out.println("              Men�            ");
			System.out.println("1.- Ficheros Secuenciales");
			System.out.println("2.- Ficheros Aleatorios");
			System.out.println("3.- Salir de la aplicaci�n");
			System.out.println("*******************************************");
			System.out.println("Selecciona una opci�n");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				int opcion2;
				do {
					System.out.println("*******************************************");
					System.out.println("        Men� Ficheros Secuenciales            ");
					System.out.println("1.- Crear Ficheros Secuenciales");
					System.out.println("2.- Guardar Informaci�n");
					System.out.println("3.- Visualizar Fichero");
					System.out.println("4.- Buscar Jugador en el Fichero");
					System.out.println("5.- Cambiar mes n�mero a mes en letra");
					System.out.println("6.- M�ximo Goleador");
					System.out.println("7.- �Los jugadores est�n entrenando?");
					System.out.println("8.- Media de goles");
					System.out.println("9.- Mostras los jugadores mayores de 18 a�os");
					System.out.println("10.- Volver al men� principal");
					System.out.println("*******************************************");
					System.out.println("Selecciona una opci�n");
					opcion2 = input.nextInt();
					switch (opcion2) {
					case 1:
						System.out.println("Introduce el nombre del fichero");
						fichero.crearFichero();
						break;
					case 2:
						fichero.guardarFichero();
						break;
					case 3:
						fichero.visualizarFichero();
						break;
					case 4:
						System.out.println(fichero.buscarPalabra());
						break;
					case 5:
						fichero.cambiarMesAletras();
						break;
					case 6:
						fichero.maximoGoleador();
						break;
					case 7:
						fichero.entrenamiento();
						break;
					case 8:
						fichero.mediaGoles();
						break;
					case 9:
						fichero.mostrarMayoresDeEdad();
						break;
					case 10:
						System.out.println("Volviendo al men� principal.............");
						break;
					default:
						System.out.println("Opci�n no contemplada");

					}
				} while (opcion2 != 10);
				break;
			case 2:
				int opcion3;
				;
				do {
					System.out.println("*******************************************");
					System.out.println("        Men� Ficheros Aleatorios            ");
					System.out.println("1.- Escribir en Ficheros Secuenciales");
					System.out.println("2.- Visualizar Fichero Aleatorio");
					System.out.println("3.- Modificar nombre Fichero Aleatorio");
					System.out.println("4.- Mostrar el n�mero de veces que aparece el estilo Defensivo");
					System.out.println("5.- Copiar en otro fichero toda la informaci�n de los entrenadores Ofensivos");
					System.out.println("6.- Mostrar los entrenadores con un estilo de juego");
					System.out.println("7.- Cambiar apellido por *");
					System.out.println("8.- Codificar el nombre con numeros aleatorios");
					System.out.println("9.- Mostrar los bytes de una palabra");
					System.out.println("10.- Volver al men� principal");
					System.out.println("*******************************************");
					System.out.println("Selecciona una opci�n");
					opcion3 = input.nextInt();
					switch(opcion3) {
					case 1:
						archivos.rellenarArchivo();
						break;
					case 2:
						archivos.visualizarArchivo();
						break;
					case 3:
						archivos.modificarArchivo();
						break;
					case 4:
						archivos.contadorDefensivo();
						break;
					case 5:
						archivos.copiarFichero();
						break;
					case 6:
						archivos.mostrarEstiloDeJuego();
						break;
					case 7:
						archivos.reemplazarPorAsterisco();
						break;
					case 8:
						archivos.codificarNombre();
						break;
					case 9:
						archivos.codigoAscii();
						break;
					case 10:
						System.out.println("Volviendo al men� principal.............");
						break;
						default:
							System.out.println("Opci�n no contemplada");
					}
				} while (opcion3 != 10);
				break;
			case 3:
				System.exit(0);
				break;
			default:
				System.out.println("Opci�n no contemplada");
			}
		} while (opcion != 3);
		System.out.println("La aplicaci�n se ha cerrado");
		input.close();

		// FICHERO DE ACCESO ALEATORIO//
		// Pruebas prueba= new Pruebas();
		// prueba.rellenarArchivo();
		// prueba.visualizarArchivo();
		// prueba.codificarNombre();

	}

}
